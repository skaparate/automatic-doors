# Automatic Doors Showcase

This project was meant to showcase automatic doors made by a company.

I've tried to represent the models the most accurately possible as real life.

## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
