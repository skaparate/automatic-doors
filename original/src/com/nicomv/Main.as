﻿package com.nicomv  {
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.DisplayObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import com.nicomv.engine.UI;
	import com.nicomv.Utils;

	[SWF(backgroundColor = "#ffffff", frameRate = "60", width = "1024", height = "600", wmode = "direct")]
	[Frame(factoryClass = "com.nicomv.Preloader")]
	
	public class Main extends Sprite {
		private var _ui: UI			= null;
		
		public function Main() {
			super();
			
			if( ! this.stage )
				addEventListener( Event.ADDED_TO_STAGE, addedToStage );
			else
				addedToStage();
		}
		
		private function addedToStage( e: Event = null ): void {
			removeEventListener( Event.ADDED_TO_STAGE, addedToStage );
			
			var loader: URLLoader	= new URLLoader();
			loader.addEventListener( Event.COMPLETE, configLoaded );
			loader.load( new URLRequest( 'Application.xml' ) );
		}
		
		private function configLoaded( e: Event ): void {
			stage.focus		= this;
			stage.align		= StageAlign.TOP_LEFT;
			stage.scaleMode	= StageScaleMode.NO_SCALE;
			
			_ui				= new UI( this, new XML( e.target.data ) );
			_ui.setup();
			
			stage.addEventListener( Event.RESIZE, onResize );
			onResize();
		}
		
		private function onResize( e: Event = null ): void {
			_ui.bounds = {
				width: stage.stageWidth,
				height: stage.stageHeight
			};
		}
	}
	
}