package com.nicomv.doors {
	import adobe.utils.CustomActions;
	import away3d.containers.Scene3D;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.events.LoaderEvent;
	import away3d.events.AssetEvent;
	import away3d.materials.ColorMaterial;
	import com.nicomv.Utils;
	import flash.events.Event;
	import caurina.transitions.Tweener;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author ...
	 */
	public class Swing extends DoorBase {
		private var _frontSensor: Mesh		= null;
		private var _backSensor: Mesh		= null;
		private var _sensors: Vector.<Mesh>	= null;
		
		public function Swing( scene: Scene3D, picker: StaticLightPicker, scale: Array, color: Object ) {
			super( scene, picker, scale, color );
			
			_name			= 'Swing';
			_modelName		= 'Swing.obj';
			_frontSensor	= null;
			_backSensor		= null;
			_windows		= new Array();
		}
		
		override public function init(): void {
			super.init();
			
			if( ! _modelLoaded )
				loadModel();
			else
				dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function modelLoaded( e: LoaderEvent = null, cback: Function = null ): void {
			setupFrame();
			addWindows( _doors.getChildAt(0) );
			changeColor( _color );
			scaleTo( _scaleX, _scaleY, _scaleZ );
			_modelLoaded = true;
			dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function addWindows( door: ObjectContainer3D ): void {
			door.addChild( _windows[0] );
		}
		
		override protected function meshComplete( e: AssetEvent ): void {
			super.meshComplete( e );
			var mesh: Mesh	= Mesh( e.asset );
			
			if ( /Window_window/.test( mesh.name ) ) {
				trace( '--- Window Mesh Complete: ' + mesh.name );
				_windows.push( mesh );
			} else {
				var cont: ObjectContainer3D 	= new ObjectContainer3D();
				
				switch( mesh.name ) {
					case 'Door_door':
						mesh.name = cont.name = 'door';
						cont.addChild( mesh );
						_doors.addChild( cont );
						
					case 'FrontSensor_sensor':
						_frontSensor	= mesh;
						break;
					
					case 'BackSensor_sensor':
						_backSensor		= mesh;
						break;
				}
			}
		}
		
		override protected function setupFrame(): void {
			trace( 'Setup Frame' );
			super.setupFrame();
			_sensors					= new <Mesh>[ _frontSensor, _backSensor ];
			_finalFrame					= _merge.applyToMeshes( _frame, _sensors );
			_finalFrame.mouseEnabled	= true;
		}
		
		override public function changeColor( color: Object ): void {
			super.changeColor( color );
			changeFrameColor();
			_finalFrame			 		= _merge.applyToMeshes( _frame, _sensors );
		}
		
		private function changeFrameColor(): void {
			var material: ColorMaterial = ColorMaterial( _frame.material );
			material					= applyPropsToMaterial( material );
			_frame.material				= material;
		}
		
		override public function toggleDoor( lcback: Function = null, rcback: Function = null ): void {
			var d: ObjectContainer3D	= _doors.getChildAt(0);
				
			if ( ! Tweener.isTweening( d ) ) {
				var params: Object	= {
					transition: 'linear',
					time: 0.7
				};
				
				var w: Number	= (d.maxX - d.minX);
				
				if ( _doorsOpen ) {
					params.rotationY	= 0;
					
					params.onComplete	= function(): void {
						_doorsOpen	= false;
							
						if( rcback !== null )
							rcback();
								
						resetPivotPoints();
					}
					
					Tweener.addTween( d, params );
				} else {
					params.rotationY	= 90;
					params.onComplete = function(): void {
						_doorsOpen	= true;
						
						if( rcback !== null )
							rcback();
					}
					
					d.movePivot( -w / 2, 0, 0 );
					Tweener.addTween( d, params );
				}
			}
		}
		
		override public function dispose(): void {
			super.dispose();
			_frontSensor.dispose();
			_backSensor.dispose();
			_windows[0].dispose();
		}
	}

}