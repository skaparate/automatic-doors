package com.nicomv.character {
	import away3d.entities.Mesh;
	import away3d.events.AssetEvent;
	import away3d.library.AssetLibrary;
	import away3d.events.LoaderEvent;
	import away3d.loaders.misc.AssetLoaderToken;
	import away3d.loaders.parsers.Max3DSParser;
	
	import com.nicomv.Utils;
	
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class Entity {
		private var _male: Mesh				= null;
		private var _female: Mesh			= null;
		//private var _dolls: Array			= new Array();
		
		public function Entity() {
			AssetLibrary.enableParser( Max3DSParser );
		}
		
		public function getModel( copy: Boolean = false): Mesh {
			var rand: Number	= Utils.randRange( 0, 1 );
			var tmp: Mesh		= null;
			
			if ( rand == 0 )
				tmp = _male;
			else
				tmp = _female;
				
			if ( copy ) {
				return Mesh( tmp.clone() );
			}
				
			return tmp;
		}
		
		public function init(): void {
			var modelFile: String		= Utils.ASSETS_FOLDER.concat( 'models/maniqui.3ds' );
			var request: URLRequest		= new URLRequest( modelFile );
			var token: AssetLoaderToken	= AssetLibrary.load( request );
			token.addEventListener( AssetEvent.ASSET_COMPLETE, assetComplete );
			token.addEventListener( LoaderEvent.RESOURCE_COMPLETE, modelLoaded );
			token.addEventListener( LoaderEvent.LOAD_ERROR, modelLoadingError );
		}
		
		private function modelLoaded( e: LoaderEvent ): void {
			trace( 'Model ' + e.url + ' loaded!' );
			var height: Number = 0;
			var sx: Number = 8.5,
				sy: Number = 9.5,
				sz: Number = 8.5;
			
			_male	= Mesh( AssetLibrary.getAsset( 'HumanMale' ) );
			height	= (_male.maxY - _male.minY);
			_male.y	= height * 4.5;
			
			_female		= Mesh( AssetLibrary.getAsset( 'HumanFemale' ) );
			sy			= 9;
			height		= (_female.maxY - _female.minY);
			_female.y 	= height * 5;
			_male.scaleX = _female.scaleX = sx;
			_male.scaleY = _female.scaleY = sy;
			_male.scaleZ = _female.scaleZ = sz;
		}
		
		private function modelLoadingError( e: LoaderEvent ): void {
			trace( 'An error ocurred loading ' + e.url + ': ' + e.message );
		}
		
		private function assetComplete( e: AssetEvent ): void {
			trace( 'Asset complete: ' + [e.asset, e.asset.name] );
		}
	}

}