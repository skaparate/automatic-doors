﻿package com.nicomv {
	import adobe.utils.CustomActions;
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Sprite3D;
	import away3d.materials.TextureMaterial;
	import away3d.stereo.methods.InterleavedStereoRenderMethod;
	import away3d.textures.BitmapTexture;
	import away3d.utils.Cast;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	public class Utils {
		public static const ASSETS_FOLDER: String = 'assets/';
		
		public function Utils() {}
		
		public static function between( num: Number, min: Number, max: Number ): Boolean {
			var r: Boolean = false;
			
			if( ! isNaN( num ) ) {
				if( num >= min && num <= max )
					r = true;
			}
			
			return r;
		}
		
		public static function isNumber(keycode: int): Boolean {
			var r: Boolean = false;
			
			if( Utils.between( keycode, 48, 57 ) && Utils.between( keycode, 96, 105 ) )
				r = true;
			
			return r;
		}
		
		public static function traceObj( obj: Object ): void {
			trace( 'Tracing Object: ' + obj );
			
			for( var val:* in obj ){
				trace( '   [' + typeof( obj[val] ) + '] ' + val + ' => ' + obj[val] );
			}
		}
		
		public static function getChildByName( parent: ObjectContainer3D, name: String ): * {
			trace( 'Get child by name...' );
			var len: int = parent.numChildren,
				 el: *	 = null,
				  i: int = 0;
				  
			for ( i; i < len; i++ ) {
				var t: * = parent.getChildAt( i );
				//trace( 'Element name: ' + t.name );
				if ( t.name == name ) {
					el = t;
					break;
				}
			}
				
			if ( el === null )
				trace( 'Element "' + name + '" not found.' );
				
			return el;
		}
		
		public static function logChildren( obj: ObjectContainer3D ): void {
			if ( obj.numChildren > 0 ) {
				trace( 'Loggin Children of ' + obj );
				var i: int = 0;
				for ( i; i < obj.numChildren; i++ ) {
					trace( obj.getChildAt( i ).name );
				}
			}
		}
		
		public static function randRange( minNum: Number, maxNum: Number ): Number {
            return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
        }
		
		public static function drawText( text: String, format: TextFormat = null, readOnly: Boolean = false ): Sprite3D {
			var txtFormat: TextFormat	= new TextFormat();
			
			if ( format != null ) {
				txtFormat = format;
			} else {
				txtFormat.size		= 9;
				txtFormat.color		= 0;
				txtFormat.kerning	= true;
			}
			
			var txtField: TextField	= new TextField();
			txtField.text			= text;
			txtField.width			= 64;
			txtField.height			= 32;
			
			if ( readOnly )
				txtField.type		= TextFieldType.DYNAMIC;
			else
				txtField.type		= TextFieldType.INPUT;
			
			var bmData: BitmapData	= new BitmapData( txtField.width, txtField.height, true, 0x00ffffff );
			bmData.draw( txtField );
			
			var material: TextureMaterial	= new TextureMaterial( Cast.bitmapTexture( bmData ) );
			material.alpha					= 0.8;
			material.alphaBlending			= true;
			
			var tmp: Sprite3D				= new Sprite3D( material, txtField.width, txtField.height );
			tmp.scale( 0.2 );
			
			return tmp;
		}
		
		/**
		 * 
		 * @param	str: String - The string to be parsed.
		 * @return	Object		- Returns the parsed string as an object.
		 */
		public static function stringToObject( str: String ): Object {
			trace( 'String to object..' );
			
			var props: Array	= Utils.trim( str ).split( ',' ),
				out: Object		= new Object();
				
			props.forEach( function( obj: * , index: int, arr: Array ): void {
				arr[index]		= Utils.trim( arr[index] );
				var tmp: Array	= arr[index].split( ':', 2 );
				out[tmp[0]]		= Utils.trim( tmp[1] );
			});
			
			Utils.traceObj( out );
			
			return out;
		}
		
		public static function trim( s: String ): String {
			return s.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
		}
	}
	
}
