package com.nicomv.engine {
	import away3d.containers.View3D;
	import com.nicomv.Utils;
	import fl.controls.Label;
	import fl.data.DataProvider;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldType;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import fl.controls.ComboBox;
	
	import caurina.transitions.Tweener;
	/**
	 * ...
	 * @author ...
	 */
	public class Controls extends Sprite {
		private var _controlsContainer: Sprite		= null;
		private var _view: View3D					= null;
		
		private var _models: XMLList				= null;
		private var _colors: XMLList				= null;
		
		private var _heightInput: TextField			= null;
		private var _widthInput: TextField			= null;
		private var _controlsShown: int				= 1; // 0 hidden, 1 visible.
		private var _ccHeight: Number				= 0;
		private var _changeBtn: Button				= null;
		private var _openBtn: Button				= null;
		private var _emergencyBtn: Button 			= null;
		
		private var _colorCombo: ComboBox			= null;
		private var _modelCombo: ComboBox			= null;
		private var _format: TextFormat				= null;
		
		private var _maxWidth: Number				= 0;
		
		public function Controls( view: View3D, models: XMLList, colors: XMLList ) {
			super();
			_view	= view;
			_models	= models;
			_colors	= colors;
			init();
		}
		
		private function init(): void {
			_format					= new TextFormat();
			_format.color			= 0xffffff;
			_format.kerning			= true;
			_format.leftMargin		= 1;
			_format.size			= 14;
			addControlsContainer();
			addModelsCombo();
			addBoundsInput();
			addColorsCombo();
			addChangeButton();
			addActionButtons();
		}
		
		private function addControlsContainer(): void {
			_controlsContainer				= new Sprite();
			_controlsContainer.graphics.beginFill( 0, 0.5 );
			_controlsContainer.graphics.drawRect( _view.x, _view.y, _view.width / 6, _view.height );
			_controlsContainer.graphics.endFill();
			
			_controlsContainer.x			= _view.x;
			_controlsContainer.y			= _view.y;
			_controlsContainer.height		= _view.height;
			_ccHeight						= _controlsContainer.height;
			addChild( _controlsContainer );
		}
		
		private function addModelsCombo(): void {
			var obj: Object			= {
				x: _controlsContainer.x + 3,
				y: _controlsContainer.y + 3,
				text: 'Seleccione un Modelo:',
				format: _format,
				readOnly: true
			};
			
			var lbl: TextField			= addTextField( obj );
			_controlsContainer.addChild( lbl );
			
			_modelCombo				= new ComboBox();
			_modelCombo.x			= lbl.x;
			_modelCombo.y			= (lbl.y + lbl.textHeight) + 3;
			_modelCombo.width		= _controlsContainer.width - 5;
			_modelCombo.name		= 'modelCombo';
			_modelCombo.textField.setStyle( 'textFormat', _format );
			_modelCombo.dropdown.setRendererStyle( 'textFormat', _format );
			
			for each( var item: XML in _models.model ) {
				_modelCombo.addItem( {
					label: item.@name,
					data: item.@value
				})
			}
			
			trace( '10%: ' + (_modelCombo.width * 0.1));
			_controlsContainer.width	= _modelCombo.width + (_modelCombo.width * 0.1);
			_controlsContainer.addChild( _modelCombo );
		}
		
		private function addBoundsInput(): void {
			var y: Number			= (_modelCombo.y + _modelCombo.height) + 6,
				x: Number			= _controlsContainer.x + 5,
				numbers: RegExp		= /[0-9]\.,/;
				
			var obj: Object			= {
				x: x,
				y: y,
				text: 'Alto (mm):',
				format: _format,
				readOnly: true
			};
			
			var lbl: TextField	= addTextField( obj );
			lbl.x				= (_controlsContainer.width / 2) - (lbl.textWidth / 1.2);
			_controlsContainer.addChild( lbl );
			
			y								= lbl.y + (lbl.textHeight * 1.2);
			_format.color					= 0;
			obj								= {
				x: x,
				y: y,
				text: '0.0',
				readOnly: false,
				format: _format,
				restrict: numbers,
				width: 100
			};
			_heightInput 					= addTextField( obj );
			_heightInput.height				= _heightInput.textHeight * 1.5;
			_heightInput.border				= true;
			_heightInput.borderColor		= 0;
			_heightInput.background			= true;
			_heightInput.backgroundColor	= 0xffffff;
			_heightInput.x					= (_controlsContainer.width / 2) - (heightInput.width / 1.5);
			_controlsContainer.addChild( _heightInput );
			
			y				= _heightInput.y + ( _heightInput.textHeight * 1.8 );
			_format.color	= 0xffffff;
			obj				= {
				x: x,
				y: y,
				text: 'Ancho (mm):',
				readOnly: true,
				format: _format
			};
			
			lbl				= addTextField( obj );
			lbl.x				= (_controlsContainer.width / 2) - (lbl.textWidth / 1.2);
			_controlsContainer.addChild( lbl );
			
			y							= lbl.y + ( lbl.textHeight * 1.2 );
			_format.color				= 0;
			obj							= {
				x: x,
				y: y,
				text: '0.0',
				readOnly: false,
				format: _format,
				restrict: numbers,
				width: 100
			};
			_widthInput 				= addTextField( obj );
			_widthInput.height			= _widthInput.textHeight * 1.5;
			_widthInput.border			= true;
			_widthInput.borderColor		= 0;
			_widthInput.background		= true;
			_widthInput.backgroundColor	= 0xffffff;
			_widthInput.x				= (_controlsContainer.width / 2) - (widthInput.width / 1.5);
			_controlsContainer.addChild( _widthInput );
		}
		
		private function addActionButtons(): void {
			_openBtn			= new Button();
			_openBtn.label		= 'Abrir Puertas';
			_openBtn.x			= (_controlsContainer.width / 2) - (_openBtn.width / 1.5);
			_openBtn.y			= _changeBtn.y + (_changeBtn.height * 1.8);
			_openBtn.buttonMode	= true;
			_openBtn.enabled	= false;
			_controlsContainer.addChild( _openBtn );
			
			_emergencyBtn			 = new Button();
			_emergencyBtn.label 	 = 'Emergencia';
			_emergencyBtn.x			 = (_controlsContainer.width / 2) - (_emergencyBtn.width / 1.5);
			_emergencyBtn.y			 = _openBtn.y + (_openBtn.height * 1.2);
			_emergencyBtn.buttonMode = true;
			_emergencyBtn.enabled	 = false;
			_controlsContainer.addChild( _emergencyBtn );
		}
		
		private function addColorsCombo(): void {
			_colorCombo			= new ComboBox();
			_colorCombo.x		= _controlsContainer.x + 3;
			_colorCombo.y		= _widthInput.y + (_widthInput.textHeight * 2.5);
			_colorCombo.width	= _modelCombo.width;
			_colorCombo.name	= 'colorCombo';
			
			_colorCombo.textField.setStyle( 'textFormat', _format );
			_colorCombo.dropdown.setRendererStyle( 'textFormat', _format );
			
			for each( var item: XML in _colors.color ) {
				_colorCombo.addItem({
					label: item.@name,
					data: item.@value
				});
			}
			
			_controlsContainer.addChild( _colorCombo );
		}
		
		private function addChangeButton(): void {
			_changeBtn				= new Button();
			_changeBtn.label		= 'Aplicar';
			_changeBtn.buttonMode	= true;
			_changeBtn.x			= (_controlsContainer.width / 2) - (_changeBtn.width / 1.5);
			_changeBtn.y			= _colorCombo.y + (_colorCombo.height * 1.8);
			_controlsContainer.addChild( _changeBtn );
		}
		
		/**
		 * 
		 * 
		 * object {
		 * 	text: String = '',
		 * 	instanceName: String = '',
		 * 	x: Number = 0,
		 * 	y: Number = 0,
		 * 	readOnly: Boolean = true,
		 * 	format: TextFormat
		 * }
		 */
		public function addTextField( obj: Object ): TextField {
			var tf: TextField	= new TextField();
			
			if ( typeof(obj.text) == 'string' )
				tf.text			= obj.text;
				
			if ( typeof(obj.instanceName) == 'string' )
				tf.name			= obj.instanceName;
				
			if ( typeof(obj.x) == 'number' )
				tf.x				= obj.x;
				
			if ( typeof(obj.y) == 'number' )
				tf.y			= obj.y;
				
			if ( typeof(obj.readOnly) == 'boolean' && obj.readOnly == false )
				tf.type			= TextFieldType.INPUT;
				
			if ( typeof obj.format != 'undefined' )
				tf.setTextFormat( obj.format );
				
			if ( typeof obj.restrict != 'undefined' )
				tf.restrict	= obj.restrict;
				
			if ( typeof (obj.width) != 'undefined' )
				tf.width	= obj.width;
			else
				tf.width	= tf.textWidth + (tf.textWidth * 0.1);
				
			return tf;
		}
		
		public function get modelCombo(): ComboBox {
			return _modelCombo;
		}
	
		public function get openButton(): Button {
			return _openBtn;
		}
		
		public function get emergencyButton(): Button {
			return _emergencyBtn;
		}
	
		public function get changeButton(): Button {
			return _changeBtn;
		}
	
		public function get heightInput(): TextField {
			return _heightInput;
		}
		
		public function get widthInput(): TextField {
			return _widthInput;
		}
	
		public function get colorCombo(): ComboBox {
			return _colorCombo;
		}
	}

}