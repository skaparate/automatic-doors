package com.nicomv {
	import away3d.library.AssetLibrary;
	import away3d.library.assets.IAsset;
	import away3d.loaders.misc.AssetLoaderToken;
	import away3d.loaders.parsers.Max3DSParser;
	import away3d.loaders.parsers.OBJParser;
	import away3d.events.LoaderEvent;
	import away3d.events.AssetEvent;
	import away3d.library.utils.AssetLibraryIterator;
	
	import flash.net.URLRequest;
	import flash.filesystem.File;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import com.nicomv.Utils;
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class ModelLoader extends EventDispatcher {
		public static const DONE: String					= 'done';
		public static const NOT_FOUND: String			= 'not_found';
		
		private var _afolder: String							= '';
		private var _models: Array								= null;
		private var _model: String								= '';
		
		public function ModelLoader() {
			_afolder = File.applicationDirectory.nativePath + File.separator + Utils.ASSETS_FOLDER + 'models/';
			_models	 = new Array();
			AssetLibrary.enableParsers( new <Class>[Max3DSParser, OBJParser] );
		}
		
		public function load( model: String ): void {
			readModels();
			_model = model.toLowerCase();
			
			var req: URLRequest = new URLRequest();
			var index: int = _models.indexOf( _model );
			if ( index > -1 ) {
				req.url = new File( _afolder + _model ).url;
				var token: AssetLoaderToken = AssetLibrary.load( req );
				token.addEventListener( LoaderEvent.LOAD_ERROR, onError );
				token.addEventListener( LoaderEvent.RESOURCE_COMPLETE, resourceComplete );
				token.addEventListener( AssetEvent.ASSET_COMPLETE, assetComplete );
				token.addEventListener( AssetEvent.ASSET_RENAME, assetRename );
				token.addEventListener( AssetEvent.GEOMETRY_COMPLETE, geometryComplete );
				token.addEventListener( AssetEvent.MESH_COMPLETE, meshComplete );
				token.addEventListener( AssetEvent.MATERIAL_COMPLETE, materialComplete );
			} else {
				trace( 'Model ' + model + ' not found!!' );
				dispatchEvent( new Event( ModelLoader.NOT_FOUND ) );
			}
		}
		
		private function readModels(): void {
			trace( 'Reading model Folder...' );
			var arr: Array = new File( _afolder).getDirectoryListing();
				
			arr.forEach(function( element: File , index: int, array: Array ): void {
				_models.push( element.name );
			});
		}
		
		private function onError( e: LoaderEvent ): void {
			trace( 'Model loading error: ' + e.message );
		}
		
		private function resourceComplete( e: LoaderEvent ): void {
			trace( 'Resouce ' + e.target + ' Complete' );
			iterateAssets();
			dispatchEvent( new Event( ModelLoader.DONE ) );
		}
		
		private function assetComplete( e: AssetEvent ): void {
			trace( 'Asset Complete: ' + e.asset.name );
		}
		
		private function assetRename( e: AssetEvent ): void {
			trace( 'Rename Asset: ' + e.assetPrevName );
		}
		
		private function geometryComplete( e: AssetEvent ): void {
			trace( 'Geometry Complete: ' + e.asset.name );
		}
		
		private function meshComplete( e: AssetEvent ): void {
			trace( 'Mesh Complete: ' + e.asset.name );
		}
		
		private function materialComplete( e: AssetEvent ): void {
			trace( 'Material Complete: ' + e.asset.name );
		}
		
		public function get currentModel(): String {
			return _model;
		}
		
		private function iterateAssets(): void {
			trace( '--- Current Assets ---' );
			var li: AssetLibraryIterator = AssetLibrary.createIterator();
			var obj: * = null;
			
			while ( obj = li.next() ) {
				trace( 'Asset Name and Type: ' + [obj.name, obj.assetType] );
			}
		}
	}

}