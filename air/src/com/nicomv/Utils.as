﻿package com.nicomv {
	import away3d.containers.ObjectContainer3D;
	
	public class Utils {
		public static const ASSETS_FOLDER: String = 'assets/';
		
		public function Utils() {}
		
		public static function between( num: Number, min: Number, max: Number ): Boolean {
			var r: Boolean = false;
			
			if( ! isNaN( num ) ) {
				if( num >= min && num <= max )
					r = true;
			}
			
			return r;
		}
		
		public static function isNumber(keycode: int): Boolean {
			var r: Boolean = false;
			
			if( Utils.between( keycode, 48, 57 ) && Utils.between( keycode, 96, 105 ) )
				r = true;
			
			return r;
		}
		
		public static function traceObj( obj: Object ): void {
			trace( 'Tracing Object: ' + obj );
			
			for( var val:* in obj ){
				trace( '   [' + typeof( obj[val] ) + '] ' + val + ' => ' + obj[val] );
			}
		}
		
		public static function getChildByName( parent: ObjectContainer3D, name: String ): * {
			var len: int = parent.numChildren,
				 el: *	 = null,
				  i: int = 0;
				  
			for ( i; i < len; i++ ) {
				var t: * = parent.getChildAt( i );
					
				if ( t.name == name ) {
					el = t;
					break;
				}
			}
				
			if ( el === null )
				trace( 'Element "' + name + '" not found.' );
				
			return el;
		}
		
		public static function logChildren( obj: ObjectContainer3D ): void {
			if ( obj.numChildren > 0 ) {
				trace( 'Loggin Children of ' + obj );
				var i: int = 0;
				for ( i; i < obj.numChildren; i++ ) {
					trace( obj.getChildAt( i ).name );
				}
			}
		}
	}
	
}
