﻿package com.nicomv.engine {
	
	import adobe.utils.CustomActions;
	import away3d.containers.Scene3D;
	import away3d.core.partition.CameraNode;
	import com.nicomv.engine.Input;
	import flash.geom.Vector3D;
	
	import flash.display.Sprite;
	import flash.ui.Keyboard;
	
	import away3d.cameras.Camera3D;
	import away3d.containers.ObjectContainer3D;
	import away3d.controllers.LookAtController;
	import away3d.cameras.lenses.PerspectiveLens;
	import away3d.cameras.lenses.OrthographicLens;
	
	public class Camera extends Input {
		private var _debug: Boolean								= false;
		private var _parent: Sprite								= null;
		private var _cam: Camera3D								= null;
		private var _control: LookAtController 		= null;
		private var _zSens: Number								= 1.0;
		private var _xSens: Number								= 2.0;
		private var _ySens: Number								= 2.0;
		
		public function Camera( canvas: Sprite, bounds: Array = null, debug: Boolean = false ) {
			super( canvas );
			_debug 		= debug;
			_parent		= canvas;
			_cam 			= new Camera3D();
			_cam.lens = new PerspectiveLens( 90 );
			_cam.x 		= 0;
			_cam.y 		= 30;
			_cam.z 		= -70;
			_control 	= new LookAtController( _cam );
			_cam.lookAt( new Vector3D( 0, 0, 0 ) );
			
		}
		
		public function get cam3D(): Camera3D {
			return _cam;
		}
		
		public function listen(): void {
			if( isKeyDown ) {
				switch( lastKey ) {
					case Keyboard.SPACE:
							_cam.position = new Vector3D( 0, 0, -70 );
							break;
					case Keyboard.UP:
						//if( _cam.z < 100 )
							_cam.moveForward( _zSens );
						break;
						
					case Keyboard.DOWN:
						//if( _cam.z > -100 )
							_cam.moveBackward( _zSens );
						break;
						
					case Keyboard.A:
						_cam.moveLeft( _xSens );
						break;
						
					case Keyboard.D:
							_cam.moveRight( _xSens );
						break;
						
					case Keyboard.W:
							_cam.moveUp( _ySens );
						break;
						
					case Keyboard.S:
							if( ( _cam.y - 22 ) > 0 )
								_cam.moveDown( _ySens );
						break;
				}
				
				trace( 'Cam.vector: ' + [_cam.x, _cam.y, _cam.z] );
			}
			
			_control.update();
		}
		
		public function setSensibility( x: Number = 2.0, y: Number = 2.0, z: Number = 1 ): void {
			_xSens = x;
			_ySens = y;
			_zSens = z;
		}
		
		public function get xSens(): Number {
			return _xSens;
		}
		
		public function get ySens(): Number {
			return _ySens;
		}
		
		public function get zSens(): Number {
			return _zSens;
		}
		
		public function changeZ( p: Object ): void {
			_cam.x = p.x;
			_cam.y = p.y;
			_cam.z = p.z;
		}
		
		public function set target( obj: ObjectContainer3D ): void {
			_control.lookAtObject = obj;
		}
	}
	
}
