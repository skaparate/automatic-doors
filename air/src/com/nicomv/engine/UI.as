﻿package com.nicomv.engine {
	import adobe.utils.CustomActions;
	import away3d.filters.tasks.Filter3DDoubleBufferCopyTask;
	import away3d.library.assets.AssetType;
	import away3d.materials.methods.FilteredShadowMapMethod;
	import away3d.materials.TextureMaterial;
	import away3d.textures.BitmapTexture;
	import com.nicomv.doors.DuraGlide3k;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	
	// NicoMV imports:
	import com.nicomv.engine.Camera;
	import com.nicomv.Utils;
	
	// Away 3D imports:
	import away3d.cameras.Camera3D;
	import away3d.containers.View3D;
	import away3d.containers.Scene3D;
	import away3d.entities.Mesh;
	import away3d.materials.ColorMaterial;
	import away3d.primitives.ConeGeometry;
	import away3d.primitives.CubeGeometry;
	import away3d.primitives.SphereGeometry;
	import away3d.primitives.CylinderGeometry;
	import away3d.core.math.Plane3D;
	import away3d.lights.DirectionalLight;
	import away3d.lights.PointLight;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.primitives.PlaneGeometry;
	import away3d.containers.ObjectContainer3D;
	import away3d.events.MouseEvent3D;
	import away3d.core.base.Geometry;
	import away3d.core.base.SubMesh;
	import away3d.core.base.SubGeometry;
	import away3d.library.AssetLibrary;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.loaders.parsers.OBJParser;
	import away3d.loaders.parsers.DAEParser;
	import away3d.loaders.parsers.Max3DSParser;
	import away3d.loaders.parsers.ParserBase;
	import away3d.library.utils.AssetLibraryIterator;
	import away3d.library.assets.IAsset;
	import away3d.debug.AwayStats;
	import away3d.entities.SegmentSet;
	import away3d.primitives.LineSegment;
	import away3d.tools.commands.*;
	import away3d.utils.Cast;
	import away3d.tools.utils.Bounds;
	
	import caurina.transitions.Tweener;
	import com.nicomv.ModelLoader;
	
	public class UI {
		[Embed(source="../../../../embeds/floor_texture.png")]
		public static var FloorTexture: Class;
		
		private var _c: Sprite									= null;
		private var _scene: Scene3D								= null;
		private var _view: View3D								= null;
		private var _cam: Camera								= null;
		private var _dg3k: DuraGlide3k							= null;
		
		private var _dbgTxt: TextField							= null;
		private var _debug: Boolean								= false;
		
		// Lights:
		private var _lpicker: StaticLightPicker 				= null;
		private var _sun: DirectionalLight						= null;
		private var _rLight: PointLight							= null;
		private var _lLight: PointLight							= null;
		
		private var _merge: Merge								= null;
		
		//private var _mater: Object							= null;
		private var _fMaterial: TextureMaterial = null;
		private var _ground: Mesh								= null;
		private var _closeTime: int								= 0;
		
		public function UI( canvas: Sprite, debug: Boolean = false ) {
			_c		 = canvas;
			_debug = debug;
		}
		
		public function setup(): void {
			trace( 'SETTING UP' );
			// Initializes variables:
			_scene	 				= new Scene3D();
			_cam		 			= new Camera( _c, null, _debug );
			_merge 	 				= new Merge( true );
			//_mater	 			= new Object();
			
			setupView();
			lights();
			
			if( _debug ) {
				_c.addChild( new AwayStats( _view, true, false, 0, true, true ) );
				setupDbgTxt();
			}
			
			_c.addEventListener( Event.ENTER_FRAME, update );
			drawScene();
		}
		
		private function setupView(): void {
			_view									= new View3D();
			_view.scene						= _scene;
			_view.camera					= _cam.cam3D;
			_view.backgroundColor = 0xdedede;
			_view.renderer.antiAlias = 4;
			_c.addChild( _view );
		}
		
		private function lights(): void {
			_sun 				 			= new DirectionalLight( -1, -1, -1 );
			_sun.specular 		= 0.3;
			_sun.ambient			= 1;
			_sun.diffuse  		= 1;
			
			_rLight						= new PointLight();
			_rLight.radius 		= 50;
			_rLight.fallOff		= 100;
			_rLight.position	= new Vector3D( 50, 50, -20 );
			
			_lLight						= new PointLight();
			_lLight.radius 		= 50;
			_lLight.fallOff		= 100;
			_lLight.position	= new Vector3D( 50, 50, +20 );
			
			_scene.addChild( _sun );
			_scene.addChild( _rLight );
			_scene.addChild( _lLight );
			
			_lpicker 					= new StaticLightPicker( [_sun, _rLight, _lLight] );
		}
		
		private function setupDbgTxt(): void {
			if ( _debug ) {
				_dbgTxt										= new TextField();
				_dbgTxt.background				= true;
				_dbgTxt.backgroundColor		= 0x000000;
				_dbgTxt.textColor					= 0xffffff;
				_dbgTxt.multiline					= true;
				_dbgTxt.type							= TextFieldType.DYNAMIC;
				_dbgTxt.x 								= 0;
				_dbgTxt.mouseWheelEnabled = true;
				_dbgTxt.width 						= 500;
				_dbgTxt.alpha							= 0.7;
				_dbgTxt.y			= _view.height - ( _view.height * 1 );
				_dbgTxt.width	= _view.width;
				_c.addChild( _dbgTxt );
			}
		}
		
		private function drawScene(): void {
			addFrontFloor();
			_dg3k	= new DuraGlide3k();
			_dg3k.init();
		}
		
		/*private function setupMaterials(): void {
			_mater.steel 				= ColorMaterial( AssetLibrary.getAsset( 'Steel' ) );
			_mater.glass 				= ColorMaterial( AssetLibrary.getAsset( 'Glass' ) );
			_mater.black				= ColorMaterial( AssetLibrary.getAsset( 'Black' ) );
			_mater.black.smooth 		= _mater.glass.smooth = _mater.steel.smooth = true;
			_mater.steel.lightPicker	= _lpicker;
			_mater.steel.ambient		= 0.3;
			_mater.black.lightPicker 	= _lpicker;
			_mater.glass.lightPicker 	= _lpicker;
			_mater.glass.ambient		= 0.8;
			_mater.glass.alpha			= 0.2;
		}*/
		
		private function drawAxis(): void {
			var lset: SegmentSet = new SegmentSet();
			// Positive Axis
			var x: Number			= 0,
					len: Number		= 5,
					thick: Number = 2;
			
			var line: LineSegment = new LineSegment(
				new Vector3D( x, 0, 0 ),
				new Vector3D( x + len, 0, 0 ),
				0xFF0000,
				0xFF0000,
				thick
			);
			lset.addSegment( line );
			
			line = new LineSegment(
				new Vector3D( x, 0, 0 ),
				new Vector3D( x, len, 0 ),
				0x00FF00,
				0x00FF00,
				thick
			);
			lset.addSegment( line );
			
			line = new LineSegment(
				new Vector3D( x, 0, 0 ),
				new Vector3D( x, 0, len ),
				0x0000FF,
				0x0000FF,
				thick
			);
			lset.addSegment( line );
			
			// Negative Axis
			/*new LineSegment(
				new Vector3D( 20, 0, 0 ),
				new Vector3D( 25, 0, 0 ),
				0xFF0000,
				0xFF0000
			);
			lset.addSegment( line );
			
			line = new LineSegment(
				new Vector3D( 15, 0, 0 ),
				new Vector3D( 15, -5, 0 ),
				0x00FF00,
				0x00FF00
			);
			lset.addSegment( line );
			
			line = new LineSegment(
				new Vector3D( 15, 0, 0 ),
				new Vector3D( 15, 0, -5 ),
				0x0000FF,
				0x0000FF
			);
			lset.addSegment( line );*/
			
			_scene.addChild( lset );
		}
		
		private function addFrontFloor(): void {
			_fMaterial = new TextureMaterial( Cast.bitmapTexture( FloorTexture ) );
			_fMaterial.shadowMethod = new FilteredShadowMapMethod( _sun );
			_fMaterial.lightPicker	= _lpicker;
			_fMaterial.specular			= 0;
			
			_ground		= new Mesh( new PlaneGeometry( 300, 300 ), _fMaterial );
			_ground.y = 0; //-17.5;
			_scene.addChild( _ground );
		}
		
		private function objClick( e: MouseEvent3D ): void {
			log( 'Object clicked name: ' + e.target.name );
			_cam.target = e.target as ObjectContainer3D;
		}
		
		public function set bounds(	dim: Object ): void {
			trace( 'Setting view bounds: ' + [dim.width, dim.height] );
			
			if( _debug ) {
				_dbgTxt.width = dim.width;
				_dbgTxt.y			= dim.height - ( dim.height * 0.1 );
			}
			
			_view.width	= dim.width;
			_view.height	= dim.height;
		}
		
		private function update( e: Event ): void {
			var ms: Number = new Date().time / 1000;
			_cam.listen();
			_view.render();
		}
		
		public function log( msg: String ) : void {
			if ( _debug && _dbgTxt != null ) {
				_dbgTxt.appendText( msg.concat( '\n' ) );
			}
		}
		
		/*private function drawText( txt: String, color: uint = 0xffffff ): BitmapTexture {
			var tf: TextField = new TextField();
			var format: TextFormat = new TextFormat( 'droidSans' );
			
			tf.embedFonts = true;
			tf.text = txt;
			tf.defaultTextFormat = format;
			tf.selectable = false;
			
			var bmData: BitmapData = new BitmapData( tf.width, tf.height, false, color );
			bmData.draw( tf );
			
			var bmTexture: BitmapTexture = new BitmapTexture( bmData );
		
			
			var bmap: Bitmap = new Bitmap( bmData );
			bmap.smoothing = true;
			
			return bmap;
		}*/
	}
	
}
