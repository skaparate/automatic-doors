package com.nicomv {
	
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
  import flash.display.MovieClip;
  import flash.events.ProgressEvent;
	import flash.text.TextField;
  import flash.utils.getDefinitionByName;
	import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
	import flash.display.StageScaleMode;
	import flash.display.StageAlign;
	import flash.display.DisplayObject;

  public class Preloader extends MovieClip {
		private var _txt: TextField = null;
		
		public function Preloader() {
			_txt 					 = new TextField();
			
			if (stage) {
				stage.scaleMode 	= StageScaleMode.NO_SCALE;
				stage.align				= StageAlign.TOP_LEFT;
				stage.stageWidth	= 800;
				stage.stageHeight = 600;
				_txt.x				 		= stage.stageWidth / 2.2;
				_txt.y				 		= stage.stageHeight / 2;
			}
			
			addEventListener( Event.ENTER_FRAME, checkFrame );
			loaderInfo.addEventListener( ProgressEvent.PROGRESS, progress );
			loaderInfo.addEventListener( IOErrorEvent.IO_ERROR, ioError );
			_txt.textColor = 0x000000;
			addChild( _txt );
		}
		
		private function ioError( e: IOErrorEvent ): void {
			trace(e.text);
		}
		
		private function progress( e: ProgressEvent ): void {
			// TODO update loader
			var p: Number = ( e.bytesLoaded * 100 ) / e.bytesTotal;
			_txt.text = 'Cargado: ' + p.toFixed( 2 );
		}
		
		private function checkFrame( e: Event ): void {
			if (currentFrame == totalFrames) {
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished(): void {
			removeEventListener( Event.ENTER_FRAME, checkFrame );
			loaderInfo.removeEventListener( ProgressEvent.PROGRESS, progress );
			loaderInfo.removeEventListener( IOErrorEvent.IO_ERROR, ioError );
			
			// TODO hide loader
			removeChild( _txt );
			startup();
		}
		
		private function startup(): void {
			var App:Class = getDefinitionByName( "com.nicomv.Main" ) as Class;
			addChild( new App() as DisplayObject );
		}
		
	}
}