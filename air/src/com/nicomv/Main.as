﻿package com.nicomv  {
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.DisplayObject;
	import flash.net.URLRequest;
	import flash.filesystem.File;
	
	//import com.nicomv.I18n;
	import com.nicomv.engine.UI;
	import com.nicomv.Utils;

	[SWF(backgroundColor = "#dedede", frameRate = "60", width = "800", height = "600", wmode = "direct")]
	[Frame(factoryClass="com.nicomv.Preloader")]
	public class Main extends Sprite {
		/*private var mLangList: Array = null;
		private var mLangDir: String = File.applicationDirectory.nativePath + '/lang';
		private var mi18n: I18n = null;*/
		
		private var _ui: UI											= null;
		
		public function Main() {
			super();
			
			if( ! this.stage )
				addEventListener( Event.ADDED_TO_STAGE, addedToStage );
			else
				addedToStage();
		}
		
		private function addedToStage( e: Event = null ): void {
			removeEventListener( Event.ADDED_TO_STAGE, addedToStage );
			stage.focus 			= this;
			stage.align				= StageAlign.TOP_LEFT;
			stage.scaleMode		= StageScaleMode.NO_SCALE;
			_ui								= new UI( this, false );
			_ui.setup();
			stage.addEventListener( Event.RESIZE, onResize );
			onResize();
		}
		
		private function onResize( e: Event = null ): void {
			_ui.bounds = { width: stage.stageWidth, height: stage.stageHeight };
		}
		
		/*public function getLangOptions(): Array {
			if(mLangList == null)
				this.readLangs();
				
			var arr: Array = new Array();
			
			for(var i in this.mLangList) {
				arr.push({label: this.mi18n.read(this.mLangList[i]), data: this.mLangList.name});
			}
		}
		
		public function readLangs(): Array {
			trace('ReadLangs');
			var file: File = new File(this.mLangDir);
			this.mLangList = file.getDirectoryListing();
			
			return this.mLangList;
		}*/
	}
	
}