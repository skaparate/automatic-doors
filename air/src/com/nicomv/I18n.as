﻿package com.nicomv {
	public class I18n {
		public const MISSING_REQUIRED_PARAM: int = -1;
		
		private var mTpath: String				= '';
		private var mLang: String					= '';
		private var mTranslation: Object	= null;
		private var mUtil: Utils					= null;
		
		/**
			* @param Options Array - An array with the configuration.
				Options {
					'bpath' => Where the translation XML files are,
					'lang'	=> Language to choose.
			*/
		public function I18n(options: Object) {
			trace('I18n Constructor - Parameters:');
			Utils.traceObj(options);
			if(options['bpath'] == undefined)
				throwError(this.MISSING_REQUIRED_PARAM, 'bpath');
			else
				this.mTpath = options['bpath'];
				
			var re: RegExp = /[a-z]{2}_[a-z]{2}/gi;
			if(options['lang'] == undefined || ! re.test(options['lang']))
				throwError(this.MISSING_REQUIRED_PARAM, 'lang');
			else
				this.mLang = options['lang'];
				
			this.readLangFile();
		}
		
		private function readLangFile(): void {
			switch(this.mLang) {
				default:
					this.parseFile();
					break;
			}
		}
		
		private function parseFile(): void {
			
		}
		
		private function throwError(code: int, extra: String): void {
			var arr: Array = new Array();
			
			switch(code) {
				case this.MISSING_REQUIRED_PARAM:
					arr.push('Missing the required param "' + extra + '".');
					arr.push(this.MISSING_REQUIRED_PARAM);
					break;
					
				default:
					break;
			}
			
			throw new Error(arr[0], arr[1]);
		}

	}
	
}
