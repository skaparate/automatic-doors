package com.nicomv.doors {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.tools.commands.Merge;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.library.AssetLibrary;
	import away3d.loaders.misc.AssetLoaderToken;
	import away3d.loaders.parsers.Max3DSParser;
	import away3d.loaders.parsers.OBJParser;
	import flash.net.URLRequest;
	
	
	import caurina.transitions.Tweener;
	
	import flash.geom.Vector3D;
	
	import com.nicomv.Utils;
	
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	internal class DoorBase {
		protected var _name: String				= '';
		protected var _modelName: String 		= '';
		protected var _frame: ObjectContainer3D	= null;
		
		function DoorBase( name: String, model: String ) {
			_name		= name;
			_modelName	= model;
			_frame		= new ObjectContainer3D();
			AssetLibrary.enableParser( Max3DSParser );
			AssetLibrary.enableParser( OBJParser );
		}
		
		protected function loadModel(): void {
			trace( 'Loading model...' );
			var modelFile: String		= Utils.ASSETS_FOLDER.concat( _modelName );
			var request: URLRequest		= new URLRequest( modelFile );
			var token: AssetLoaderToken	= AssetLibrary.load( request );
			token.addEventListener( AssetEvent.ASSET_COMPLETE, assetComplete );
			token.addEventListener( LoaderEvent.RESOURCE_COMPLETE, modelLoaded );
			token.addEventListener( LoaderEvent.LOAD_ERROR, modelLoadingError );
		}
		
		protected function modelLoaded( e: LoaderEvent ): void {
			trace( 'Model ' + e.url + ' loaded!' );
			setupFrame();
		}
		
		protected function modelLoadingError( e: LoaderEvent ): void {
			trace( 'An error ocurred loading ' + e.url + ': ' + e.message );
		}
		
		protected function assetComplete( e: AssetEvent ): void {
			trace( 'Asset complete: ' + e.asset );
		}
		
		protected function setupFrame(): void {
			trace( 'Setting up frame...' );
		}
		
		protected function setupDoors(): void {
			trace( 'Setting up doors...' );
		}
		
		protected function setupWindows( parent: ObjectContainer3D ): void {
			trace( 'Setting up windows... ' );
		}
		
		protected function openDoor( lcback: Function = null, rcback: Function = null ): void {
			trace( 'Opening door... ' );
		}
		
		protected function closeDoor( lcback: Function = null, rcback: Function = null ): void {
			trace( 'Closing door...' );
		}
		
		protected function checkAnimation( cameraPos: Vector3D ): void {
			trace( 'Checking animation... ' );
		}
		
		protected function set name( name: String ): void {
			if( name.length > 0 )
				_name = name;
		}
		
		protected function set modelName( model: String ): void {
			if ( model.length > 0 )
				_modelName = model;
		}
		
		public function get name(): String {
			return _name;
		}
		
		public function get modelName(): String {
			return _modelName;
		}
	}
}