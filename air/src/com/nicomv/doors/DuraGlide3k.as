package com.nicomv.doors {
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class DuraGlide3k extends DoorBase {
		
		public function DuraGlide3k() {
			super( 'Dura Glide 3000', 'DuraGlide3k.3ds' );
		}
		
		public function init(): void {
			loadModel();
		}
		
		override protected function setupFrame(): void {
			trace( 'DuraGlide3k.setupFrame' );
		}
	}

}